<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\PostsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function () {
    //posts
    Route::get('/posts', [PostsController::class, 'index'])->name('api.posts');
    Route::get('/my-posts', [PostsController::class, 'myIndex'])->name('api.my.posts');
    Route::get('/post/{id}', [PostsController::class, 'single'])->name('api.post');
    Route::post('/posts', [PostsController::class, 'create'])->name('api.create.post');
    Route::put('/posts/{id}', [PostsController::class, 'update'])->name('api.update.post');
    Route::delete('/posts/{id}', [PostsController::class, 'destroy'])->name('api.delete.post');

    //user
    Route::post('/logout', [UserController::class, 'logout'])->name('api.logout');
});

Route::post('/register', [UserController::class, 'register'])->name('api.register');
Route::post('/login', [UserController::class, 'login'])->name('api.login');
