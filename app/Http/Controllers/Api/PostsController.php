<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Validator;
use Auth;

class PostsController extends Controller
{
    /**
     * Display all posts.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $posts = Posts::all();
        return response()->json(['success' => true, 'posts' => $posts]);
    }

    /**
     * Display current user posts.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myIndex(): JsonResponse
    {
        $posts = Posts::where('user_id', auth()->user()->id)->get();
        return response()->json(['success' => true, 'posts' => $posts]);
    }

    /**
     * Create post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $rules = [
            'title'     => ['required', 'string', 'max:191'],
            'slug'      => ['nullable', 'string', 'max:191'],
            'body'      => ['required', 'string'],
            'image_url' => ['nullable', 'string', 'max:191'],
        ];

        $customMessages = [
            'required' => 'The :attribute field is required.',
            'max' => 'The :attribute max length is 191 character.',
            'string' => 'The :attribute type must be string.',
        ];

        $validator = Validator::make( $request->all(), $rules, $customMessages );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'post' => $validator->errors()->first()]);
        }

        $slugChecker = Posts::where('slug', $request->slug)->first();

        if (isset($request->slug) && !$slugChecker) {
            $slug = $request->slug;
        }else{
            $slug = $this->generateSlug($request->title);
        }

        $post = Posts::create([
            'title'     => $request->title,
            'body'      => $request->body,
            'slug'      => $slug,
            'image_url' => $request->image_url,
            'user_id'   => auth()->user()->id,
        ]);

        return response()->json(['success' => true, 'post' => $post]);
    }

    /**
     * Update post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $rules = [
            'title'     => ['string', 'max:191'],
            'slug'      => ['string', 'max:191'],
            'body'      => ['string'],
            'image_url' => ['string', 'max:191'],
        ];

        $customMessages = [
            'max' => 'The :attribute max length is 191 character.',
            'string' => 'The :attribute type must be string.',
        ];

        $validator = Validator::make( $request->all(), $rules, $customMessages );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'post' => $validator->errors()->first()]);
        }

        $postChecker = Posts::where('id', $id)->where('user_id', auth()->user()->id)->first();

        if (!$postChecker) {
            return response()->json(['success' => false, 'message' => "Post either doesn't exist or you don't have permisions to edit it."]);
        }

        if (isset($request->slug)) {
            $slugChecker = Posts::where('id', '<>', $id)->where('slug', $request->slug)->first();
            if ($slugChecker) {
                $request->slug = $request->slug . Str::random(5) . '-' . time();
            }
        }

        Posts::where('id', $id)->update($request->toArray());

        $post = Posts::where('id', $id)->get();

        return response()->json(['success' => true, 'post' => $post]);

    }

    /**
     * Delete post.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $postChecker = Posts::where('id', $id)->where('user_id', auth()->user()->id)->first();

        if (!$postChecker) {
            return response()->json(['success' => false, 'message' => "Post either doesn't exist or you don't have permisions to delete it."]);
        }

        Posts::where('id', $id)->delete();
        return response()->json(['success' => true]);
    }

    /**
     * Generate unique slug.
     *
     * @param  string $name
     * @return string
     */
    protected function generateSlug(string $name): string
    {
        $delimiter = '-';
        $slug = strtolower(
            trim(
                preg_replace('/[\s-]+/', $delimiter,
                    preg_replace('/[^A-Za-z0-9-]+/', $delimiter,
                        preg_replace('/[&]/', 'and',
                            preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $name))
                        )
                    )
                ),
                $delimiter
            )
        );
        $slugChecker = Posts::where('slug', $slug)->first();
        if (!$slugChecker) {
            return $slug;
        }

        return $slug . $delimiter . Str::random(5) . $delimiter . time();
    }

    /**
     * Show single post.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function single(int $id): JsonResponse
    {
        $post = Posts::where('id', $id)->first();
        return response()->json(['success' => true, 'post' => $post]);
    }
}
