<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use \Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Validator;
use Auth;

class UserController extends Controller
{
    /**
     * Register User
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $rules = [
            'name'      => ['required', 'string', 'max:191'],
            'email'     => ['unique:users', 'required', 'email', 'max:191'],
            'password'  => ['required', 'min:8', 'string'],
        ];

        $customMessages = [
            'required' => 'The :attribute field is required.',
            'max' => 'The :attribute max length is 191 character.',
            'min' => 'The :attribute min length is 8 character.',
            'string' => 'The :attribute type must be string.',
            'email' => 'The :attribute type must be valid email.',
            'unique' => 'The :attribute type must be unique.',
        ];

        $validator = Validator::make( $request->all(), $rules, $customMessages );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'post' => $validator->errors()->first()]);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'remember_token' => Str::random(10),
        ]);

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        return response()->json(['success' => true, 'user' => $user , 'token' => $token]);
    }

    /**
     * User Login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $rules = [
            'email'     => ['required', 'email', 'max:191'],
            'password'  => ['required', 'min:8', 'string'],
        ];

        $customMessages = [
            'required' => 'The :attribute field is required.',
            'max' => 'The :attribute max length is 191 character.',
            'min' => 'The :attribute min length is 8 character.',
            'string' => 'The :attribute type must be string.',
            'email' => 'The :attribute type must be valid email.',
        ];

        $validator = Validator::make( $request->all(), $rules, $customMessages );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'post' => $validator->errors()->first()]);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['success' => false, "message" =>'User does not exist']);
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->json(['success' => false, "message" => "Password mismatch"]);
        }

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        return response()->json(['success' => true, 'token' => $token]);
    }

    /**
     * User Logout
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $token = $request->user()->token();
        $token->revoke();
        return response()->json(['success' => true, 'message' => 'You have been successfully logged out!']);
    }
}
