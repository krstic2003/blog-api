<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use App\Models\Posts;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Laravel\Passport\Passport;

class PostControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testPostCreation(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payload = [
            'title' => 'Post 123',
            'body' => 'Post body',
            'slug' => 'post-123',
            'image_url' => 'dsdsds',
            'user_id' => $user->id
        ];

        $this->postJson(route('api.create.post'), $payload)
            ->assertSuccessful()
            ->assertJsonStructure([
                'success',
                'post',
            ]);

        $this->assertDatabaseHas('posts', [
            'title' => $payload['title'],
            'body' => $payload['body'],
            'slug' => $payload['slug'],
            'image_url' => $payload['image_url'],
            'user_id' => $user->id,
        ]);
    }

    public function testPostCreationWitoutSlug(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payload = [
            'title' => 'Post 123',
            'body' => 'Post body',
            'image_url' => 'dsdsds',
            'user_id' => $user->id
        ];

        $this->postJson(route('api.create.post'), $payload)
            ->assertSuccessful()
            ->assertJsonStructure([
                'success',
                'post',
            ]);

        $this->assertDatabaseHas('posts', [
            'title' => $payload['title'],
            'body' => $payload['body'],
            'image_url' => $payload['image_url'],
            'user_id' => $user->id,
        ]);
    }

    public function testPostCreationWitoutTitle(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payload = [
            'body' => 'Post body',
            'image_url' => 'dsdsds',
            'user_id' => $user->id
        ];

        $expected = [
            "success" => false,
            "post" => "The title field is required."
        ];

        $response = $this->postJson(route('api.create.post'), $payload)
            ->assertStatus(200);

        $this->assertEquals($expected, json_decode($response->getContent(), true));
    }

    public function testGetAllPosts(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $this->getJson(route('api.posts'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'success',
            ]);
    }

    public function testGetMyPosts(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $this->getJson(route('api.posts'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'success',
            ]);
    }

    public function testGetSinglePost(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $this->getJson(route('api.post', ['id' => '123']))
            ->assertSuccessful()
            ->assertJsonStructure([
                'success',
            ]);
    }

    public function testPostUpdate(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payloadCreate = [
            'title' => 'Post 1234',
            'body' => 'Post body',
            'slug' => 'post-1234',
            'image_url' => 'dsdsds',
            'user_id' => $user->id,
        ];

        $payload = [
            'title' => 'Post 123',
            'body' => 'Post bod',
            'slug' => 'post-123',
            'image_url' => 'ds',
        ];

        $post = Posts::create($payloadCreate);

        $this->putJson(route('api.update.post', ['id' => $post->id]), $payload)
            ->assertSuccessful()
            ->assertJsonStructure([
                'success',
                'post',
            ]);

        $this->assertDatabaseHas('posts', [
            'title' => $payload['title'],
            'body' => $payload['body'],
            'slug' => $payload['slug'],
            'image_url' => $payload['image_url'],
            'user_id' => $user->id,
        ]);
    }

    public function testPostUpdatePartial(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payloadCreate = [
            'title' => 'Post 1234',
            'body' => 'Post body',
            'slug' => 'post-1234',
            'image_url' => 'dsdsds',
            'user_id' => $user->id,
        ];

        $payload = [
            'body' => 'Post bod',
        ];

        $post = Posts::create($payloadCreate);

        $this->putJson(route('api.update.post', ['id' => $post->id]), $payload)
            ->assertSuccessful()
            ->assertJsonStructure([
                'success',
                'post',
            ]);

        $this->assertDatabaseHas('posts', [
            'title' => $payloadCreate['title'],
            'body' => $payload['body'],
            'slug' => $payloadCreate['slug'],
            'image_url' => $payloadCreate['image_url'],
            'user_id' => $user->id,
        ]);
    }

    public function testPostDelete(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payloadCreate = [
            'title' => 'Post 1234',
            'body' => 'Post body',
            'slug' => 'post-1234',
            'image_url' => 'dsdsds',
            'user_id' => $user->id,
        ];

        $post = Posts::create($payloadCreate);

        $response = $this->deleteJson(route('api.delete.post', ['id' => $post->id]))
            ->assertStatus(200);

        $expected = [
            "success" => true
        ];

        $this->assertEquals($expected, json_decode($response->getContent(), true));
    }

    public function testPostDeleteNoPost(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $response = $this->deleteJson(route('api.delete.post', ['id' => '123']))
            ->assertStatus(200);

        $expected = [
            "success" => false,
            "message" => "Post either doesn't exist or you don't have permisions to delete it."
        ];

        $this->assertEquals($expected, json_decode($response->getContent(), true));
    }
}
