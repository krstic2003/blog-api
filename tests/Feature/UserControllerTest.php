<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Laravel\Passport\Passport;

class UserControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testSuccessfulRegistration(): void
    {
        $this->withoutExceptionHandling();

        $payload = [
            'name' => $this->faker->firstName,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
        ];

        $this->postJson(route('api.register'), $payload)->assertSuccessful();

        User::where('email', $payload['email'])->firstOrFail();

        $this->assertDatabaseHas('users', [
            'email' => $payload['email'],
        ]);
    }

    public function testSuccessfulLogin(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payload = [
            'email' => $user->getEmailForPasswordReset(),
            'password' => 'password',
        ];

        $this->postJson(route('api.login'), $payload)
            ->assertSuccessful()
            ->assertJsonStructure([
                'token',
            ]);

        $this->assertAuthenticatedAs($user, 'api');
    }

    public function testInvalidCredentials(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $payload = [
            'email' => $user->getEmailForPasswordReset(),
            'password' => 'random@123',
        ];

        $expected = [
            "success" => false,
            "message" => "Password mismatch"
        ];

        $response = $this->postJson(route('api.login'), $payload)
            ->assertStatus(200);

        $this->assertEquals($expected, json_decode($response->getContent(), true));
    }

    public function testSuccessfulLogout(): void
    {
        $user = Passport::actingAs(
            User::factory()->create()
        );

        $response = $this->postJson(route('api.logout'));
        $response->assertSuccessful();
    }
}
